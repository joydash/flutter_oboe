import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:ffi/ffi.dart';

import 'dart:ffi';
import 'dart:io';

//How C Function looks like and how it's going to be exposed to dart
typedef flutter_oboe_native_add = Int32 Function(Int32 a, Int32 b); //Typedef for C
typedef FlutterOboeNativeAdd = int Function(int a, int b); //Typedef for dart

//Oboe Type defs
//Audio stream
typedef oboe_stream_init = Pointer<Void> Function();
typedef OboeStreamInit = Pointer<Void> Function();

typedef oboe_stream_dispose = Void Function(Pointer<Void>);
typedef OboeStreamDispose= void Function(Pointer<Void>);

typedef oboe_stream_sample_rate = Int32 Function(Pointer<Void>);
typedef OboeStreamSampleRate = int Function(Pointer<Void>);

typedef oboe_stream_start_stop = Void Function(Pointer<Void>);
typedef OboeStreamStartStop = void Function(Pointer<Void>);

typedef oboe_stream_write = Void Function(Pointer<Void>, Pointer<Float>, Int32);
typedef OboeStreamWrite = void Function(Pointer<Void>, Pointer<Float>, int);

//Audio recorder
typedef oboe_start_test_recording = Void Function(Pointer<Void>, Pointer<Utf8>, Int32);
typedef OboeStartTestRecording = void Function(Pointer<Void>, Pointer<Utf8>, int);

typedef oboe_stop_test_recording = Void Function(Pointer<Void>);
typedef OboeStopTestRecording = void Function(Pointer<Void>);

class FlutterOboeTest {
  //Using as a dart style type def
  final nativeLib = Platform.isAndroid ? DynamicLibrary.open("liboboeffi.so") : DynamicLibrary.process();

  //Test function
  late FlutterOboeNativeAdd? _nativeAdd;

  //Getting the channel from the package name which is "flutter_oboe"
  static const MethodChannel _channel = MethodChannel('flutter_oboe');
  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  //Singleton class so that it can be called with FlutterObe()
  static final FlutterOboeTest _instance =  FlutterOboeTest._();
  factory FlutterOboeTest(){
    return _instance;
  }

  //Audio stream
  OboeStreamInit? _streamInit;
  OboeStreamDispose? _streamDispose;
  OboeStreamSampleRate? _streamSampleRate;
  OboeStreamStartStop? _streamStart;
  OboeStreamStartStop? _streamStop;
  OboeStreamWrite? _streamWrite;

  //Audio recording
  OboeStartTestRecording? _startTestRecording;
  OboeStopTestRecording? _stopTestRecording;

  //Android build the native code as a shared object(<lib_name>.so)
  //XCode toolchain complile the native code directly into the main executable and can just call process method
  FlutterOboeTest._(){
    //Now we're looking up a method that's a native function which has the signature of the C-style typedef ("sum") and casting that as a function to dart style type def
    _nativeAdd = nativeLib.lookup<NativeFunction<flutter_oboe_native_add>>('native_add').asFunction();

    //Funcions exported from oboeffiexport.cpp class
    //Look up C Functions and store the references as dart functions
    //Audio Stream
    _streamInit = nativeLib.lookup<NativeFunction<oboe_stream_init>>('stream_create').asFunction();
    _streamDispose = nativeLib.lookup<NativeFunction<oboe_stream_dispose>>('stream_create').asFunction();
    _streamDispose = nativeLib.lookup<NativeFunction<oboe_stream_dispose>>('stream_dispose').asFunction();
    _streamSampleRate = nativeLib.lookup<NativeFunction<oboe_stream_sample_rate>>('stream_sample_rate').asFunction();
    _streamStart = nativeLib.lookup<NativeFunction<oboe_stream_start_stop>>('stream_start').asFunction();
    _streamStop = nativeLib.lookup<NativeFunction<oboe_stream_start_stop>>('stream_stop').asFunction();
    _streamWrite = nativeLib.lookup<NativeFunction<oboe_stream_write>>('stream_write').asFunction();

    //Audio recorder
    _startTestRecording = nativeLib.lookup<NativeFunction<oboe_start_test_recording>>('start_recording').asFunction();
    _stopTestRecording = nativeLib.lookup<NativeFunction<oboe_stop_test_recording>>('stop_recording').asFunction();
  }
}

//Wrapper to access from public
class OboeTestStream{
  late Pointer<Void>? _nativeInstance;
  OboeTestStream(){
    _nativeInstance = FlutterOboeTest()._streamInit!();
  }

  void dispose(){
    FlutterOboeTest()._streamDispose!(_nativeInstance!);
  }

  //Testings
  int performNativeAdd(int a, int b){
    return FlutterOboeTest()._nativeAdd!(a, b);
  }

  //Audio Streams
  int getSampleRate(){
    return FlutterOboeTest()._streamSampleRate!(_nativeInstance!);
  }

  void start(){
    return FlutterOboeTest()._streamStart!(_nativeInstance!);
  }

  void stop(){
    return FlutterOboeTest()._streamStop!(_nativeInstance!);
  }

  void write(Float32List original){
    var length = original.length;
    var copy = calloc.allocate<Float>(length)..asTypedList(length);
    // copy.
    // ..setAll(0, original);
    FlutterOboeTest()._streamWrite!(_nativeInstance!, copy, length);
    calloc.free(copy);
  }

  //Audio Recorder
  void startTestRecording(String path, int recordingFrequency){
    print('Recording started');
    FlutterOboeTest()._startTestRecording!(_nativeInstance!, path.toNativeUtf8(), recordingFrequency);
  }

  void stopTestRecording(){
    print('Recording stopped');
    FlutterOboeTest()._stopTestRecording!(_nativeInstance!);
  }


}

import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:ffi/ffi.dart';

import 'dart:ffi';
import 'dart:io';

//How C Function looks like and how it's going to be exposed to dart
typedef flutter_oboe_native_add = Int32 Function(Int32 a, Int32 b); //Typedef for C
typedef FlutterOboeNativeAdd = int Function(int a, int b); //Typedef for dart

//Oboe Type defs
//Initialisations
typedef oboe_engine_create = Pointer<Void> Function();
typedef OboeEngineCreate = Pointer<Void> Function();

typedef oboe_engine_dispose = Void Function(Pointer<Void>);
typedef OboeEngineDispose= void Function(Pointer<Void>);

//Recorder
typedef oboe_engine_start_recording = Void Function(Pointer<Void>);
typedef OboeEngineStartRecording = void Function(Pointer<Void>);

typedef oboe_engine_pause_recording = Void Function(Pointer<Void>);
typedef OboeEnginePauseRecording = void Function(Pointer<Void>);

typedef oboe_engine_resume_recording = Void Function(Pointer<Void>);
typedef OboeEngineResumeRecording = void Function(Pointer<Void>);

typedef oboe_engine_stop_recording = Void Function(Pointer<Void>);
typedef OboeEngineStopRecording = void Function(Pointer<Void>);

typedef oboe_engine_write_to_file = Void Function(Pointer<Void>, Pointer<Utf8>);
typedef OboeEngineWriteToFile = void Function(Pointer<Void>, Pointer<Utf8>);

//Stream
typedef oboe_engine_play_recorded_stream = Void Function(Pointer<Void>);
typedef OboeEnginePlayRecordedStream = void Function(Pointer<Void>);

typedef oboe_engine_pause_recorded_stream = Void Function(Pointer<Void>);
typedef OboeEnginePauseRecordedStream = void Function(Pointer<Void>);

typedef oboe_engine_resume_recorded_stream = Void Function(Pointer<Void>);
typedef OboeEngineResumeRecordedStream = void Function(Pointer<Void>);

typedef oboe_engine_stop_recorded_stream = Void Function(Pointer<Void>);
typedef OboeEngineStopRecordedStream = void Function(Pointer<Void>);

//Player
typedef oboe_engine_play_from_file = Void Function(Pointer<Void>, Pointer<Utf8>);
typedef OboeEnginePlayFromFile = void Function(Pointer<Void>, Pointer<Utf8>);

typedef oboe_engine_pause_playing_from_file = Void Function(Pointer<Void>);
typedef OboeEnginePausePlayingFromFile = void Function(Pointer<Void>);

typedef oboe_engine_resume_playing_from_file = Void Function(Pointer<Void>);
typedef OboeEngineResumePlayingFromFile = void Function(Pointer<Void>);

typedef oboe_engine_stop_playing_from_file = Void Function(Pointer<Void>);
typedef OboeEngineStopPlayingFromFile = void Function(Pointer<Void>);

class OboeAudioEngine {
  final nativeLib = Platform.isAndroid ? DynamicLibrary.open("liboboeffi.so") : DynamicLibrary.process();

  //Singleton class so that it can be called with FlutterObe()
  static final OboeAudioEngine _instance =  OboeAudioEngine._();
  factory OboeAudioEngine(){
    // if(_instance == null) {
    //   _instance = OboeAudioEngine._();
    // }
    return _instance;
  }

  //Audio stream
  OboeEngineCreate? _engineCreate;
  OboeEngineDispose? _engineDispose;

  //Audio recording
  OboeEngineStartRecording? _engineStartRecording;
  OboeEnginePauseRecording? _enginePauseRecording;
  OboeEngineResumeRecording? _engineResumeRecording;
  OboeEngineStopRecording? _engineStopRecording;
  OboeEngineWriteToFile? _engineWriteToFile;

  //Stream
  OboeEnginePlayRecordedStream? _enginePlayRecordedStream;
  OboeEnginePauseRecordedStream? _enginePauseRecordedStream;
  OboeEngineResumeRecordedStream? _engineResumeRecordedStream;
  OboeEngineStopRecordedStream? _engineStopRecordedStream;

  //Audio player
  OboeEnginePlayFromFile? _enginePlayFromFile;
  OboeEnginePausePlayingFromFile? _enginePausePlayingFromFile;
  OboeEngineResumePlayingFromFile? _engineResumePlayingFromFile;
  OboeEngineStopPlayingFromFile? _engineStopPlayingFromFile;

  //Android build the native code as a shared object(<lib_name>.so)
  //XCode toolchain complile the native code directly into the main executable and can just call process method
  OboeAudioEngine._(){
    _engineCreate = nativeLib.lookup<NativeFunction<oboe_engine_create>>('oboe_create').asFunction();
    _engineDispose = nativeLib.lookup<NativeFunction<oboe_engine_dispose>>('oboe_dispose').asFunction();

    //Audio recorder
    _engineStartRecording = nativeLib.lookup<NativeFunction<oboe_engine_start_recording>>('start_recording').asFunction();
    _enginePauseRecording = nativeLib.lookup<NativeFunction<oboe_engine_pause_recording>>('pause_recording').asFunction();
    _engineResumeRecording = nativeLib.lookup<NativeFunction<oboe_engine_resume_recording>>('resume_recording').asFunction();
    _engineStopRecording = nativeLib.lookup<NativeFunction<oboe_engine_stop_recording>>('stop_recording').asFunction();
    _engineWriteToFile = nativeLib.lookup<NativeFunction<oboe_engine_write_to_file>>('write_to_file').asFunction();

    //Stream
    _enginePlayRecordedStream = nativeLib.lookup<NativeFunction<oboe_engine_play_recorded_stream>>('play_recorded_stream').asFunction();
    _enginePauseRecordedStream = nativeLib.lookup<NativeFunction<oboe_engine_pause_recorded_stream>>('pause_recorded_stream').asFunction();
    _engineResumeRecordedStream = nativeLib.lookup<NativeFunction<oboe_engine_resume_recorded_stream>>('resume_recorded_stream').asFunction();
    _engineStopRecordedStream = nativeLib.lookup<NativeFunction<oboe_engine_stop_recorded_stream>>('stop_recorded_stream').asFunction();

    //Audio player
    _enginePlayFromFile = nativeLib.lookup<NativeFunction<oboe_engine_play_from_file>>('play_from_file').asFunction();
    _enginePausePlayingFromFile = nativeLib.lookup<NativeFunction<oboe_engine_pause_playing_from_file>>('pause_playing_from_file').asFunction();
    _engineResumePlayingFromFile = nativeLib.lookup<NativeFunction<oboe_engine_resume_playing_from_file>>('resume_playing_from_file').asFunction();
    _engineStopPlayingFromFile = nativeLib.lookup<NativeFunction<oboe_engine_stop_playing_from_file>>('stop_playing_from_file').asFunction();
  }
}

//Wrapper to access from public
class FlutterOboe{
  late Pointer<Void>? _nativeInstance;
  FlutterOboe(){
    _nativeInstance = OboeAudioEngine()._engineCreate!();
  }

  void dispose(){
    OboeAudioEngine()._engineDispose!(_nativeInstance!);
  }

  //Audio Recorder
  void startRecording(){
    OboeAudioEngine()._engineStartRecording!(_nativeInstance!);
  }

  void pauseRecording(){
    OboeAudioEngine()._enginePauseRecording!(_nativeInstance!);
  }

  void resumeRecording(){
    OboeAudioEngine()._engineResumeRecording!(_nativeInstance!);
  }

  void stopRecording(){
    OboeAudioEngine()._engineStopRecording!(_nativeInstance!);
  }

  void writeToFile(String path)
  {
    OboeAudioEngine()._engineWriteToFile!(_nativeInstance!,  path.toNativeUtf8());
  }

  //Stream
  void playRecordedStream()
  {
    OboeAudioEngine()._enginePlayRecordedStream!(_nativeInstance!);
  }

  void pauseRecordedStream(){
    OboeAudioEngine()._enginePauseRecordedStream!(_nativeInstance!);
  }

  void resumeRecordedStream(){
    OboeAudioEngine()._engineResumeRecordedStream!(_nativeInstance!);
  }

  void stopRecordedStream(){
    OboeAudioEngine()._engineStopRecordedStream!(_nativeInstance!);
  }

  //Audio Player
  void playFromFile(String path)
  {
    OboeAudioEngine()._enginePlayFromFile!(_nativeInstance!,  path.toNativeUtf8());
  }

  void pausePlayingFromFile(){
    OboeAudioEngine()._enginePausePlayingFromFile!(_nativeInstance!);
  }

  void resumePlayingFromFile(){
    OboeAudioEngine()._engineResumePlayingFromFile!(_nativeInstance!);
  }

  void stopPlayingFromFile(){
    OboeAudioEngine()._engineStopPlayingFromFile!(_nativeInstance!);
  }
}

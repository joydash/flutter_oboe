# flutter_oboe

Oboe audio engine plugin for android!

## Getting Started
This project is a basic sample of how to use Oboe audio engine from google inside the flutter project.

Note: Oboe is not available for iOS yet so that you need to handle things seperately if you're building from iOS.


Configuration requirements for android
If you're using path_provider package, make sure yoour target and compile SDLK For the android is 31
Note: Make sure you add the correct permissions or it will not record the sound.

<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.STORAGE"/>

<!--For Karaoke-->
<uses-permission android:name="android.permission.RECORD_AUDIO"/>
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
<uses-permission android:name="android.permission.BLUETOOTH" />
<uses-permission android:name="Manifest.permission.CAPTURE_AUDIO_OUTPUT" />

1 . Set up the NDK path to build with android NDK for native C/C++ codes.

2 . Make sure that you have oboe and libsndfile directories inside
"C:\flutter\.pub-cache\hosted\pub.dartlang.org" folder or whatever your flutter installation directory is






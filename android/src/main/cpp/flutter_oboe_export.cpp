#include "audio_engine/AudioEngine.h"

#ifdef __cplusplus
    #define EXTERNC extern "C"
#else
    #define EXTERNC
#endif

//C-styles exports

//Audio Streams
EXTERNC void *oboe_create(){
    return new AudioEngine();
}

EXTERNC void oboe_dispose(void* ptr){
    auto engine = static_cast<AudioEngine *>(ptr);
    //engine->closeStream();
    delete engine;
}

EXTERNC void start_recording(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->startRecording();
}

EXTERNC void pause_recording(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->pauseRecording();
}

EXTERNC void resume_recording(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->resumeRecording();
}

EXTERNC void stop_recording(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->stopRecording();
}

EXTERNC void play_recorded_stream(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->playRecordedStream();
}

EXTERNC void pause_recorded_stream(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->pauseRecordedStream();
}

EXTERNC void resume_recorded_stream(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->resumeRecordedStream();
}

EXTERNC void stop_recorded_stream(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->stopRecordedStream();
}

EXTERNC void play_from_file(void* ptr, char *filePath){
    //char path[] = "/data/user/0/com.joydash.flutter_oboe_example/app_flutter/test1.wav";
    //printf("%s\n", filePath);
    return (static_cast<AudioEngine *>(ptr))->startPlayingFromFile(filePath);
}

EXTERNC void pause_playing_from_file(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->pausePlayingFromFile();
}

EXTERNC void resume_playing_from_file(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->resumePlayingFromFile();
}

EXTERNC void stop_playing_from_file(void* ptr){
    return (static_cast<AudioEngine *>(ptr))->stopPlayingFromFile();
}

EXTERNC void write_to_file(void* ptr, char *filePath){
    //printf("Saving to : %s\n", filePath);
    //char path[6];
    //strcpy(test2, test);
    //char path[] = "/data/user/0/com.joydash.flutter_oboe_example/app_flutter/test1.wav";
   return (static_cast<AudioEngine *>(ptr))->writeToFile(filePath);
}

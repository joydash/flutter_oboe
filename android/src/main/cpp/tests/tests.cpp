//
// Created by Thet Naing Swe on 12/8/2021.
//

//Delcare integer types to be compatible with both C and C++
//Because FFI is C based plugins and if we need to use C++, we need to declare it
#include <stdint.h>

#ifdef __cplusplus
    #define EXTERNC extern "C"
#else
    #define EXTERNC
#endif

EXTERNC int32_t sum(int32_t x, int32_t y){
    return x + y;
}


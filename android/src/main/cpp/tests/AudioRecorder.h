//
// Created by Thet Naing Swe on 12/8/2021.
//

#ifndef FLUTTER_OBOE_AUDIORECORDER_H
#define FLUTTER_OBOE_AUDIORECORDER_H

#include <jni.h>
#include <string>
#include <oboe/Oboe.h>
#include <fstream>
#include "../debug-utils/logging_macros.h"
//#include "Synthesizer.h"

namespace little_endian_io
{
    template <typename Word>
    std::ostream& write_word( std::ostream& outs, Word value, unsigned size = sizeof( Word ) )
    {
        for (; size; --size, value >>= 8)
            outs.put( static_cast <char> (value & 0xFF) );
        return outs;
    }
}

using namespace little_endian_io;
class AudioRecorder: public oboe::AudioStreamCallback {
private:
    oboe::ManagedStream outStream;
    oboe::AudioStream *stream{};

    oboe::DataCallbackResult
    onAudioReady(oboe::AudioStream *oboeStream, void *audioData, int32_t numFrames) override {
        //synth.render(static_cast<float *>(audioDaa), numFrames);
        //return DataCallbackResult::Continue;
        LOGE("onAudioReady");
    }

    static AudioRecorder *singleton;
    explicit AudioRecorder() = default;
    //Synthesizer synth;

public:
    static AudioRecorder *get() {
        if (singleton == nullptr)
            singleton = new AudioRecorder();
        return singleton;
    }

    bool isRecording = true;
    void stopAudioRecorder();
    void startAudioRecorder(const char *fullPathToFile, const int recordingFreq);
};


#endif //FLUTTER_OBOE_AUDIORECORDER_H

//
// Created by Thet Naing Swe on 12/8/2021.
//

#include "AudioEngine.h"
#include "../src/common/OboeDebug.h"

PerformanceMode::LowLatency
void AudioEngine::start(){
    AudioStreamBuilder builder;
    builder.setDirection(Direction::Input);
    builder.setPerformanceMode(PerformanceMode::LowLatency);

    AudioStream *stream;
    Result r = builder.openStream(&stream);
    if(r != Result::OK){
        LOGE("Error opening stream: %s", convertToText(r));
    }

    r = stream->requestStart();
    if(r != Result::OK){
        LOGE("Error opening stream: %s", convertToText(r));
    }

    //Using a blocking read
    //Oboe provide a bunch of const values for working with different time units
    constexpr int kMillisecondsToRecord = 2;
    const int32_t requestedFrames = (int32_t)(kMillisecondsToRecord * (stream->getSampleRate() / kMillisPerSecond));
    int16_t myBuffer[requestedFrames];

    //Time out should be longer than the amount of data we want to read
    constexpr int64_t kTimeoutValue = 3 * kNanosPerMillisecond;

    //Clear out any stale data which might already be in the stream buffer with a non-blocking read
    int framesRead = 0;
    do{
        auto result = stream->read(myBuffer, stream->getBufferSizeInFrames(), 0);
        if(result != Result::OK) break;
        framesRead = result.value();
    }while(framesRead != 0);

    while(!isRecording){
        auto result = stream->read(myBuffer, requestedFrames, kTimeoutValue);
        if(result == Result::OK){
           LOGE("Read %d frames.", result.value());
         }
         else{
             LOGE("Error reading stream: %s", convertToText(result.error()));
         }
    }

    //Close the stream when the recording is done
    stream->close();

}

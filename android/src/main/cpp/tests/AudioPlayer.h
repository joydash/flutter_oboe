//
// Created by Thet Naing Swe on 12/8/2021.
//

#ifndef FLUTTER_OBOE_AUDIOPLAYER_H
#define FLUTTER_OBOE_AUDIOPLAYER_H

#include "Synthesizer.h"

class AudioPlayer : public AudioStreamCallback{
public:
    AudioPlayer();
    void play();

private:
    Synthesizer synth;
};

#endif //FLUTTER_OBOE_AUDIOPLAYER_H

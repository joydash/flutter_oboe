//
// Created by Thet Naing Swe on 12/8/2021.
//

#ifndef FLUTTER_OBOE_AUDIOENGINE_H
#define FLUTTER_OBOE_AUDIOENGINE_H

#include "oboe/Oboe.h"

class AudioEngine {
public:
    AudioEngine();
    void start();

};

#endif //FLUTTER_OBOE_AUDIOENGINE_H

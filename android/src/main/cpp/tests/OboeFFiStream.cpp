//
// Created by Thet Naing Swe on 12/8/2021.
//

#include "OboeFFiStream.h"

OboeFFiStream::OboeFFiStream(){
    oboe::AudioStreamBuilder builder;
    builder.setFormat(oboe::AudioFormat::Float)->setChannelCount(oboe::ChannelCount::Mono);

    oboe::Result result = builder.openManagedStream(managedStream);
}

int32_t OboeFFiStream::getSampleRate(){
    return managedStream->getSampleRate();
}

void OboeFFiStream::close(){
    managedStream->close();
}
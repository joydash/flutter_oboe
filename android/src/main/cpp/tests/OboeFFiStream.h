//
// Created by Thet Naing Swe on 12/8/2021.
//

#ifndef ANDROID_OBOEFFISTREAM_H
#define ANDROID_OBOEFFISTREAM_H

#include "oboe/Oboe.h"

class OboeFFiStream {
public:
    OboeFFiStream();
    int32_t getSampleRate();
    void close();

private:
    oboe::ManagedStream managedStream;
};


#endif //ANDROID_OBOEFFISTREAM_H

//
// Created by Thet Naing Swe on 12/8/2021.
//

#include "AudioPlayer.h"
#include "../debug-utils/logging_macros.h"

void AudioPlayer::start() {

    //Create a new stream
    AudioStreamBuilder builder;

    builder.setCallback(this); //Link with the callback
    builder.setPerformanceMode(PerformanceMode::LowLatency);
    builder.setSharingMode(SharingMode::Exclusive);

    //Open the stream
    AudioStream *stream = nullptr;
    Result result = builder.openStream(&stream);

    if(result != Result::OK){
        LOGE("Error opening stream : %s", convertToText(result));
    }

    //Set the optimal buffer size to get the best results
    auto setBufferSizeResult = stream->setBufferSizeInFrames(stream->getFramesPerBurst() * 2);
    if(setBufferSizeResult){
         LOGE("New buffer size is %d frames.", setBufferSizeResult.value());
   }

    //Now send data to audio device
    result = stream->requestStart();
    if(result != Result::OK){
           LOGE("Error starting stream : %s", convertToText(result));
    }

    LOGE("Playing audio");
}

//Using callback to get high performance playback results
DataCallbackResult
AudioPlayer::onAudioReady(oboe::AudioStream *oboeStream, void *audioData, int32_t numFrames) override {
    synth.render(static_cast<float *>(audioDaa), numFrames);
    return DataCallbackResult::Continue;
    LOGE("onAudioReady");
}
#include "tests/OboeFFiStream.h"
#include "tests/AudioRecorder.h"

#ifdef __cplusplus
    #define EXTERNC extern "C"
#else
    #define EXTERNC
#endif

//C-styles exports

//Audio Streams
EXTERNC void *stream_create(){
    return new OboeFFiStream();
}

EXTERNC void stream_dispose(void* ptr){
    auto stream = static_cast<OboeFFiStream *>(ptr);
    stream->close();
    delete stream;
}

EXTERNC int32_t stream_sample_rate(void* ptr){
    return (static_cast<OboeFFiStream *>(ptr))->getSampleRate();
}

EXTERNC void stream_start(void* ptr){
    //return (static_cast<OboeFFiStream *>(ptr))->start();
}

EXTERNC void stream_stop(void* ptr){
    //return (static_cast<OboeFFiStream *>(ptr))->stop();
}

//TODO: Also include Pointer<Float>, Int32 params
EXTERNC void stream_write(void* ptr){
    //return (static_cast<OboeFFiStream *>(ptr))->stop();
}

//Audio recorder
EXTERNC void start_test_recording(void* ptr, char *filePath, int recordingFrequency){
    auto recorder = static_cast<AudioRecorder *>(ptr);
    //const char *path = (*env).GetStringUTFChars('/test.wav'c_str(), 0);
    char path[] = "/data/user/0/com.joydash.flutter_oboe_example/app_flutter/test.wav";
    //char buffer[50];
    //strcpy(buffer, str.c_str());
    printf("%s\n", filePath);

    //const char *path = '/test.wav'.c_str();
    //const int freq = (int) recordingFrequency;

    recorder->startAudioRecorder(path, recordingFrequency);
}

EXTERNC void stop_test_recording(void* ptr){
    auto recorder = static_cast<AudioRecorder *>(ptr);
    recorder->stopAudioRecorder();
}
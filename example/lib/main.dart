import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:flutter/services.dart';
import 'package:flutter_oboe/flutter_oboe.dart';
import 'package:flutter_oboe/flutter_oboe_test.dart';
import 'package:external_path/external_path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:just_audio/just_audio.dart';

import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  final stream = OboeTestStream();
  final flutterOboe = FlutterOboe();
  final noise = Float32List(512);
  late Timer timer;
  bool bFileExist = false, bRecording = false, bRecordingPaused = false;
  final _player = AudioPlayer();
  late String filePath;

  Future<String> _getPath() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    return appDocDir.path;
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();

    //Oboe Test
    for (var i = 0; i < noise.length; i++) {
      noise[i] = sin(8 * pi * i / noise.length);
    }
  }

  @override
  void dispose() {
    stream.dispose();
    flutterOboe.dispose();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = await FlutterOboeTest.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    if (!kIsWeb) {
      var status = await Permission.microphone.request();
      if (status != PermissionStatus.granted) {
        print('Microphone permission is needed');
      }
    }

    //Get the recording path
    filePath = await _getPath() + '/temp_recording.wav';

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Oboe Plugin'),
        ),
        body: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Running on: $_platformVersion').paddingOnly(bottom: 5),
              Text('Sample rate : ${stream.getSampleRate()}').paddingOnly(bottom: 5),
              ElevatedButton(
                      onPressed: startRecording,
                      style: ElevatedButton.styleFrom(
                        primary: bRecording ? Colors.blueGrey : Colors.green,
                        shadowColor: Colors.black54,
                      ),
                      child: const Text('Start recording'))
                  .paddingOnly(top: 10),

              ElevatedButton(
                  onPressed: pauseRecording,
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                    shadowColor: Colors.black54,
                  ),
                  child: const Text('Pause recording'))
                  .paddingOnly(top: 10),

                ElevatedButton(
                  onPressed: resumeRecording,
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    shadowColor: Colors.black54,
                  ),
                  child: const Text('Resume recording'))
                  .paddingOnly(top: 10),

              ElevatedButton(
                      onPressed: stopRecording,
                      style: ElevatedButton.styleFrom(
                        primary: bRecording ? Colors.red : Colors.blueGrey,
                        shadowColor: Colors.black54,
                      ),
                      child: const Text('Stop recording'))
                  .paddingOnly(bottom: 5),
              FutureBuilder(
                future: _getPath(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return Text('Recording will be saved  to: \n' + snapshot.data).paddingSymmetric(vertical: 10);
                  } else {
                    return Text("Loading").paddingSymmetric(vertical: 10);
                  }
                },
              ),
              Text(
                bFileExist ? 'Recording exist' : 'Recording not found',
                style: TextStyle(color: bFileExist ? Colors.green : Colors.red),
              ).paddingOnly(bottom: 5, top: 10),
              if (bFileExist)
                ElevatedButton(
                        onPressed: playAudio,
                        style: ElevatedButton.styleFrom(
                          primary: bRecording ? Colors.blueGrey : Colors.blue,
                          shadowColor: Colors.black54,
                        ),
                        child: const Text('Play audio'))
                    .paddingOnly(bottom: 5),

              if (bFileExist)
                ElevatedButton(
                    onPressed: pauseAudio,
                    style: ElevatedButton.styleFrom(
                      primary: bRecording ? Colors.blueGrey : Colors.blue,
                      shadowColor: Colors.black54,
                    ),
                    child: const Text('Pause audio'))
                    .paddingOnly(bottom: 5),

              if (bFileExist)
                ElevatedButton(
                    onPressed: resumeAudio,
                    style: ElevatedButton.styleFrom(
                      primary: bRecording ? Colors.blueGrey : Colors.blue,
                      shadowColor: Colors.black54,
                    ),
                    child: const Text('Resume audio'))
                    .paddingOnly(bottom: 5),

              if (bFileExist)
                ElevatedButton(
                    onPressed: stopAudio,
                    style: ElevatedButton.styleFrom(
                      primary: bRecording ? Colors.blueGrey : Colors.amber,
                      shadowColor: Colors.black54,
                    ),
                    child: const Text('Stop audio'))
                    .paddingOnly(bottom: 5),
            ],
          ),
        ),
      ),
    );
  }

  //Audio Recorder
  void startRecording() {
    if (!bRecording) {
      setState(() {
        flutterOboe.startRecording();
        bRecording = true;
      });
    }
  }

  void pauseRecording() {
    if (bRecording) {
      setState(() {
        flutterOboe.pauseRecording();
        bRecordingPaused = true;
      });
    }
  }

  void resumeRecording() {
    if (bRecordingPaused) {
      setState(() {
        flutterOboe.resumeRecording();
        bRecordingPaused = false;
      });
    }
  }

  void stopRecording() async {
    //Can stop the recording only when it's still recording
    if (bRecording) {
      setState(() {
        flutterOboe.stopRecording();
        flutterOboe.writeToFile(filePath);
        bRecording = false;
      });

      await checkFile();
    }
  }

  void saveRecording() {
    flutterOboe.writeToFile(filePath);
  }

  Future<void> checkFile() async {
    bFileExist = await File(filePath).exists();
    setState(() {
      if (bFileExist) {
        playAudio();
      }
    });
  }

  void playAudio() async {
    //Can play only when it's not recording
    if (!bRecording) {
      flutterOboe.playFromFile(filePath);
    }
  }

  void pauseAudio() async {
    //Can play only when it's not recording
    if (!bRecording) {
      flutterOboe.pausePlayingFromFile();
    }
  }

  void resumeAudio() async {
    //Can play only when it's not recording
    if (!bRecording) {
      flutterOboe.resumePlayingFromFile();
    }
  }

  void stopAudio() async {
    flutterOboe.stopPlayingFromFile();
  }
}

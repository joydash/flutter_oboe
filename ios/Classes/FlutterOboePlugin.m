#import "FlutterOboePlugin.h"
#if __has_include(<flutter_oboe/flutter_oboe-Swift.h>)
#import <flutter_oboe/flutter_oboe-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_oboe-Swift.h"
#endif

@implementation FlutterOboePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterOboePlugin registerWithRegistrar:registrar];
}
@end
